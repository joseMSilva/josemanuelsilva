defmodule Echo do

    def start() do
  		pid = spawn(Echo, :response, [])
  		send(pid, {:start, self()})
  		receive do
  			{:ok} -> :ok
  		end
  	end

  	def stop() do
  		send(:echo, {:stop, self()})
  		receive do
  			{:ok} -> :ok
  		end
  	end

  	def print(term) do
  		send(:echo, {:print, term, self()})
  		receive do
  			{:ok} -> :ok
  		end
  	end

  	def response() do
	  	receive do
	      {:start, pid} -> comenzar(pid)
	  	  {:stop, pid} -> parar(pid)
	  	  {:print, term, pid} -> imprimir(term, pid)
	  	  	
	    end
	end

	def comenzar(pid) do 
		Process.register(self(), :echo)
		send(pid, {:ok})
	    response()
	end

	def parar(pid) do
		Process.unregister(:echo)
		send(pid, {:ok})
	end

	def imprimir(term, pid) do
		
		send(pid, {:ok})
		IO.puts "#{term}"
	  	response()
	end
end