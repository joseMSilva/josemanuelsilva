defmodule Sorting do
  def quicksort([]), do: []
  def quicksort([head|tail]) do
    {lesser, greater} = Enum.split_with(tail, &(&1 < head))
    quicksort(lesser) ++ [head] ++ quicksort(greater)
  end

  def mergesort([]), do: []
  def mergesort([x]), do: [x]
  def mergesort(l) do
    f = Enum.take_every(l,2)
    s = Enum.drop_every(l,2)
    merge(mergesort(f), mergesort(s), [])
  end

  def merge(f, [], r), do: r ++ f
  def merge([], s, r), do: r ++ s
  def merge(f = [fh | ft], s = [sh | st], r) do
    cond do
      fh < sh -> merge(ft, s, r ++ [fh])
      true -> merge(f, st, r ++ [sh])
    end
  end
  
end