defmodule Manipulating do
	def filter([],y) do
		[]
	end
	def filter([h|t],y) do
		filterL([h|t],y,[])
	end
	def filterL([],y,n) do
		reverse(n)
	end
	def filterL([h|t],y,n) do
		if h<=y do
			filterL(t,y,[h|n])
		else
			filterL(t,y,n)
		end
	end
	
	def reverse([]) do
		[]
	end
	def reverse([h|t]) do
		reversel([h|t],[])
	end
	def reversel([],n) do
		n
	end
	def reversel([h|t],n) do
		reversel(t,[h|n])
	end

	def concatenate([]) do
		[]	
	end
	def concatenate([h|t]) do
		h ++ concatenate(t)
	end

	def flatten([]) do
		[]	
	end
	def flatten([h|t]) do
		flatten(h) ++ flatten(t)
	end
	def flatten(h), do: [h]

end