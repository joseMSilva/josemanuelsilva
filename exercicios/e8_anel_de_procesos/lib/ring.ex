defmodule Ring do
	def start(n,m,msg) do
		listaPids = generarAnillo(n,[])
		[h|t] = listaPids
		send(h,{:mensaje,msg})
		enviar(listaPids,m-1,msg)
		receive do
			{:ok} -> :ok
		end
	end
  	def generarAnillo(0,lista) do
    	lista
  	end
  	def generarAnillo(n,lista) do
  		pid = spawn(Ring, :recibir, [])
    	generarAnillo(n-1,lista++[pid])
  	end
  	def enviar([],0,msg) do
  		send(self(),{:ok})
		IO.puts "Todos los procesos muertos"
 	end
  	def enviar([h|t],0,msg) do
		send(h,{:muerte})
		enviar(t,0,msg)
 	end
  	def enviar([h|t],m,msg) do
		send(h,{:mensaje,msg})
		lista = t 
		primero = h
		final = lista ++ [primero]
		IO.puts "enviado a: #{inspect primero}"
		IO.puts "lista: #{inspect final}"
		enviar(final,m-1,msg)
 	end
  	def recibir() do
  		receive do
	      {:mensaje,msg} -> IO.puts "mensaje recibido: #{inspect msg}"
	      					recibir()
	      {:muerte} -> IO.puts "proceso muerto: #{inspect self()}"
	    end
  	end
end 	