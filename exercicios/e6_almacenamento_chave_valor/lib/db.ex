defmodule Db do 
	def new() do
		store = %{}
		spawn(Db, :recibir, [store])
	end

	def write(db_ref, key, element) do
		send(db_ref, {:write, db_ref, key, element, self()})
		receive do 
			{:escrito, db_ref} -> db_ref
		end
	end

	def delete(db_ref, key) do 
		send(db_ref, {:delete, db_ref, key, self()})
		receive do 
			{:eliminado, db_ref} -> db_ref
		end
	end

	def read(db_ref, key) do
		send(db_ref, {:read, key, self()})	
		receive do
			{:ok, result} -> {:ok, result}
			{:error, :not_found} ->	{:error, :not_found}
		end
	end

	def match(db_ref, element) do
		send(db_ref, {:match, element, self()})
		receive do
			{:encontrados, final_keys} -> final_keys
		end
	end

	def destroy(db_ref) do
		send(db_ref, {:destroy, db_ref, self()})
	end

	def recibir(store) do
		IO.puts"-- Database state:"
		IO.inspect store
		receive do
				{:write, db_ref, key, element, pid} -> escribirBd(pid, db_ref, key, element, store)
				{:delete, db_ref, key, pid} -> eliminarBd(db_ref, key, store, pid)
				{:read, key, pid} -> leerBd(key, store, pid)
				{:match, element, pid} -> matchBd(element, store, pid)
				{:destroy, db_ref, pid} -> destroyBd(db_ref, store, pid)
		end
	end

	def escribirBd(pid, db_ref, key, element, store) do
		store = Map.put(store, key, element)
		send(pid, {:escrito, db_ref})
		recibir(store)
	end

	def eliminarBd(db_ref, key, store, pid) do
		store1 = Map.delete(store, key)
		send(pid, {:eliminado, db_ref})
		recibir(store1)
	end
		
	def leerBd(key, store, pid) do
		result = Map.get(store, key)
		if ((inspect result) != "nil") do
			send(pid, {:ok, result})
		else
			send(pid, {:error, :not_found})
		end
		recibir(store)
	end

	def destroyBd(db_ref, store, pid) do
		Process.exit(db_ref, :kill)
		send(pid, {:ok})
	end

	def matchBd(element, store, pid) do
		list = Map.keys(store)
		comprobar(list, element, [], store, pid)
	end

	def comprobar([], element, final_keys, store, pid) do
		send(pid, {:encontrados, final_keys})
		recibir(store)
	end

	def comprobar([head|tail], element, final_keys, store, pid) do
		if Map.get(store, head) == element do
			final_keys = final_keys++[head]
			comprobar(tail, element, final_keys, store, pid)
		else
			comprobar(tail, element, final_keys, store, pid)
		end	
		
	end
end

