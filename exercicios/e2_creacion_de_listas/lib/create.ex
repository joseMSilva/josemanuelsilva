defmodule Create do 	
	def create(x) do
		createl(x,[],x)
	end
	def createl(0, l, n) do
		l
	end
	def createl(x, l, n) do
		createl(x-1,[n|l],n-1)
	end

	def reverse_create(x) do
		reverse_createl(x,[],1)
	end
	def reverse_createl(0, l, n) do
		l
	end
	def reverse_createl(x, l, n) do
		reverse_createl(x-1,[n|l],n+1)
	end
end