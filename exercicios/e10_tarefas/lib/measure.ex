defmodule Measure do
	import Manipulating
	import Sorting
	def run(lista_de_funcions, numero_de_elementos) do
		results = []
		t1 = :erlang.timestamp()
		tasks =
			for x <- 1..length(lista_de_funcions) do
				lista = []
				task = Task.async(Measure, :crearLista, [numero_de_elementos, lista])
			end
		tasks_results = Task.yield_many(tasks, 30000)
		t2 = :erlang.timestamp()
		t = :timer.now_diff(t2,t1)
		results = Enum.map(tasks_results, fn {task, {net,res}} ->
		  res || Task.shutdown(task, :brutal_kill)
		end)
		t =t/1000000
		IO.puts " -------------------------------------------"
		IO.puts "| Creacion de datos     : #{inspect t}    sec |"
		tasks2= mandarTrabajo(lista_de_funcions, results, length(lista_de_funcions), [])
		tasks_results = Task.yield_many(tasks2, 10000)
		IO.puts " -------------------------------------------"
	end

	def crearLista(0, lista) do
		lista
	end
	
	def crearLista(num, lista) do
		crearLista(num-1,lista ++ [Enum.random(0..1000)])
	end 

	def mandarTrabajo(lista_de_funcions, results, 0, tasks) do
		tasks
	end

	def mandarTrabajo(lista_de_funcions, results, num, tasks) do
		[h|t] = lista_de_funcions
		[i|j] = results
		mandarTrabajo(t,j,num-1, tasks++[Task.async(Measure, :ejecutar, [h, i])])
	end

	def ejecutar(h, i) do
		{x,y} = h
		t1 = :erlang.timestamp()
		if y == :reverse do
			x.reverse(i)
		end
		if y == :flatten do
			z = listadelistas(i,[])
			x.flatten(z)
		end
		if y == :quicksort do
			x.quicksort(i)
		end
		if y == :mergesort do
			x.mergesort(i)
		end
		t2 = :erlang.timestamp()
		ttotal = :timer.now_diff(t2,t1)
		ttotal =ttotal
		IO.puts "| #{inspect x}#{inspect y} : #{inspect ttotal}    sec |"
	end

	def listadelistas([], salida) do
		salida
	end

	def listadelistas([h|t], salida) do
		listadelistas(t, salida++[h])
	end
end
