defmodule Mi6 do
	use Agent
  	use GenServer

	def fundar() do
		GenServer.start_link(__MODULE__, :ok)
	end
	
	def recrutar(axente, destino) do
		GenServer.cast(axente, {:crear, destino})
	end

	def asignar_mision(axente, mision) do
		GenServer.cast(axente, {:asignar, mision})
	end

	def consultar_estado(axente) do 
		GenServer.call(axente, :consulta)
	end

	def disolver() do
		coger el proceso con nombre miBd
		mirar la lista e ir borrando los procesos con unregister, los agentes con stop y por ultimo mi6 con stop
		comprobar si hay que hacer eso de los dos o solo con matar uno llega
	end

	@impl true
	def init(stack) do
		Process.register(self(), :mi6)
		pid = Db.new
		Process.register(pid, :miBd)
    	{:ok, stack}
  	end

  	@impl true
  	def handle_cast({:crear, destino}, state) do
  		pid Agent.spawlink segun el numero de caracteres 
  		guardar en base de datos //usamos el ejercicio 6
  		dividir en letras desordenar lista //ejercicio2
  		Process.register(pid, :destino)
    	{:noreply, }
  	end

	@impl true
  	def handle_cast({:asignar, mision}, state) do
  		Agent.get agente con la mision
  		actualizamos su lista segun sea espiar o contraespiar //ejercicio4
  		actualizar el agente si fuera necesario
    	{:noreply, }
  	end

  	@impl true
	def handle_call(:consulta, _from, list) do
		agente.get from
		cogemos la lista y la devolvemos
		si no existe devolvemos :you_are_here_we_are_not
    	{:reply, list}
    	{:reply, :you_are_here_we_are_not}
  	end

end

cast ( server (), term ()) ::: ok
Envía una solicitud asíncrona al server.
Esta función siempre se devuelve :ok